var Dispatcher = require('../dispatchers/Dispatcher'),
    Constants = require('../constants/Constants'),
    Devices = require('../services/Devices'),
    Rules = require('../services/Rules'),
    assign = require('object-assign'),
    EventEmitter = require('events').EventEmitter
  , CHANGE_EVENT = 'change'
  , ElementDevice = false
  , OEMElements = []
  , ruleDisplay = false
  , Display = false
  , OEM = false

var AppStore = assign(EventEmitter.prototype, {
  emitChange: function() {
    this.emit(CHANGE_EVENT)
  },
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback)
  },
  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback)
  },
  getElementDevice: function() {
    return ElementDevice
  },
  getOEMElements: function() {
    return OEMElements
  },
  getDisplay: function() {
    return Display
  },
  getRuleDisplay: function() {
    return ruleDisplay
  },
  getOEM: function() {
    return OEM
  },
  dispatcherIndex: Dispatcher.register(function(payload) {
    var action = payload.action
    switch (action.actionType){ 
      case Constants.OEM: OEM = action.item; break;
      case Constants.DISPLAY: Display = action.item; break;
      case Constants.RULE_DISPLAY: ruleDisplay = action.item; break;
      case Constants.OEMELEMENTS: OEMElements = action.item; break;
      case Constants.DEVICES: Devices.set_devices(action.items); break;
      case Constants.ELEMENT_DEVICE: ElementDevice = action.item; break;
      case Constants.RULES: Rules.set_rules(action.items); break;
      case Constants.RULE: Rules.set_rule(action.item); break;
    }
    AppStore.emitChange()
    return true
  })
})

module.exports = AppStore
