'use strict'

import { Router, Route, IndexRoute, browserHistory, Redirect } from 'react-router'

var React         = require('react'),

    Access        = require('../templates/Access'),
    SignIn        = require('../components/user/SignIn'),

    Inventory     = require('../components/devices/Devices'),
    States        = require('../components/states/States'),
    Rules         = require('../components/rules/Rules'),

    Template      = require('../templates/Template'),
    NotFoundPage  = require('../templates/NotFoundPage')

module.exports = (
  <Router history={browserHistory}>
    <Route path="/" component={Access}>
      <IndexRoute component={SignIn}/>
      <Route path="/states" component={Template}>
        <IndexRoute component={States}/>
        <Route path="/inventory" component={Inventory}/>
        <Route path="/rules" component={Rules}/>
      </Route>
    </Route>
    <Redirect from="*" to="/"/>
  </Router>
)
