'use strict'

var React = require('react'),
    Bar = require('../templates/Bar'),
    Assign = require('object-assign'),
    Mixins = require('../mixins/Mixins'),
    Switch = require('../templates/Switch'),
    Actions = require('../actions/Actions'),
    Devices = require('../services/Devices'),
    FontAwesome = require('react-fontawesome')
import { Button, Glyphicon, Badge, FormControls, Col } from 'react-bootstrap'

function getDevices() { 
  let element = Devices.get_element(),
      list = []
  Devices.get_devices().map((d,i)=>{
    if (!list.length) list = [{name:d.name,elements:[d],glyph:"plus-sign"}]
    else {
      list.map((l,j)=>{
        let flag = 1
        if(d.name==l.name){
          list[j].elements.push(d)
          flag = 0
        }
        if (list.length==j+1 && flag) list.push({name:d.name,elements:[d],glyph:"plus-sign"})
      })
    }
  })
  return { devices: list, element: element }
}

// Devices.getDevices()

module.exports = React.createClass({
  mixins: [Mixins(getDevices)],
  getInitialState: function() { return { subColumns: [{name:".." ,class:""}] } },
  firstListSelection: function(i,e) {
    let devices = Assign([],this.state.devices)
    devices.map( (v,k) => { devices[k].glyph="plus-sign" } )
    devices[i].glyph = "minus-sign"
    this.setState({devices:devices,subColumns:e.elements}, function(){
      document.getElementById("second-list").style.top = (Number(i)*48)+'px'
      this.secondListSelection(0,this.state.subColumns[0])
    })
  },
  secondListSelection: function(i,e) {
    let subColumns = this.state.subColumns.map( (v,k) => { 
      v['class'] = k == i ? "btn btn-grey" : ""
      return v
    })
    this.setState({subColumns:subColumns})
    Actions.setElement([e])
  },
  componentDidMount: function(){
    this.firstListSelection(0,this.state.devices[0])
  },
  render: function() {
    let icons = {
      sensor: <FontAwesome name='rss'/>,
      light: <FontAwesome name='lightbulb-o'/>,
      actuator: <FontAwesome name='circle-o-notch'/>
    }
    return (
      <div className="component">
        <Bar first="OEM" second="OEM INFORMATION" />
        <div className="body-list">
          <div id="first-list" className="column-list">
            {this.state.devices.map((e,i)=>{
              return (
                <Button key={i} bsStyle="primary" onMouseOver={this.firstListSelection.bind(null,i,e)}>
                  <Glyphicon glyph={e.glyph}/>
                  <span className="span-st-txt">{e.name}</span>
                  <Glyphicon glyph="menu-right"/>
                  <Badge>{e.elements.length}</Badge>
                </Button>
              )
            })}
          </div>
          <div id="second-list" className="column-list">
            {this.state.subColumns.map((e,i)=>{
              return (
                <Button key={i} className={e.class} onMouseOver={this.secondListSelection.bind(null,i,e)}>
                  {e.name}
                </Button>
              )
            })}
          </div>
          <div className="inv-element">
            <form className="form-horizontal">
              {this.state.element.map((e,i)=>{
                let things = e.things == undefined ? [] : e.things
                return (
                  <div key={i}>
                    <div className="element-title">Device Information</div>
                    <FormControls.Static label="Name :" labelClassName="col-xs-5" wrapperClassName="col-xs-7" value={e.name}/>
                    <FormControls.Static label="ID :" labelClassName="col-xs-5" wrapperClassName="col-xs-7" value={e.virtualDeviceId}/>
                    <FormControls.Static label="OEM :" labelClassName="col-xs-5" wrapperClassName="col-xs-7" value={e.oem}/>
                    <FormControls.Static label="Type :" labelClassName="col-xs-5" wrapperClassName="col-xs-7" value={e.deviceType}/>
                    <div className="element-title">Things</div>
                    {things.map((e,i)=>{
                      let action = e.actions == null ? undefined : (<Switch/>)
                      return ( 
                        <div key={i} className="inv-things">
                          <div className="things-l">
                            {icons[e.thingCatagory]}
                            {e.thingCatagory}
                          </div>
                          <div className="things-r">
                            {action}
                          </div>
                        </div> )
                    })}
                  </div>
                )
              })}
            </form>
          </div>
        </div>
      </div>
    )
  }
})
