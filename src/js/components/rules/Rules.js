'use strict'

var React = require('react')
  , RulesFilter = require('./RulesFilter')
  , RulesTable = require('./RulesTable')
  , Rules = require('../../services/Rules')
  , Store = require('../../stores/Store')
  , Mixins = require('../../mixins/Mixins')
  , Rule = require('./Rule')

function getRules() {
  return {
    rules: Rules.get_rules().map( (r,i) => { return r } ),
    rule: Store.getRuleDisplay()
  }
}

Rules.loadRules()

module.exports = React.createClass({
  mixins: [Mixins(getRules)],
  render: function() {
    if(this.state.rule)
      return ( <Rule/> )
    else
      return (
        <div className="rules" id="rules">
          <RulesFilter/>
          <RulesTable rules={this.state.rules}/>
        </div>
      )
  }
})
