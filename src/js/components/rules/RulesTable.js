'use strict'

var React = require('react')
  , Actions = require('../../actions/Actions')
import { Table, Glyphicon } from 'react-bootstrap'

module.exports = React.createClass({

  thead: function() {
    return (
      <tr>
        <th column="name"><em>Name</em></th>
        <th column="if"><em>If</em></th>
        <th column="then"><em>Then</em></th>
        <th column="status"><em>Status</em></th>
        <th column="rule-link"><em></em></th>
      </tr>
    )
  },
  tbody: function() {
    return (
      <tbody>
        {this.props.rules.map((r,i)=>{
          if(r.ruleCategory=='EVENT'){
            return (
              <tr key={i}>
                <td>{r.ruleName}</td>
                <td>{r.eventCondition.property+' = '+r.eventCondition.operand}</td>
                <td>{r.eventActions[0].event||r.eventActions[0].commands[0].deviceCommands[0].commandName}</td>
                <td>{r.enabled?'true':'false'}</td>
                <td><Glyphicon className="cursor" glyph="menu-right" onClick={this.rule.bind(null,r)}/></td>
              </tr>
            )
          }
        })}
      </tbody>
    )
  },
  rule: function(r) {
    Actions.setRuleDisplay(1)
    Actions.setRule(r)
  },
  render: function() {
    let classes = "table-striped table-condensed table-hover"
    return (
      <div>
        <Table className={classes}>
          <thead>{this.thead()}</thead>
          {this.tbody()}
        </Table>
      </div>
    )
  }
})
