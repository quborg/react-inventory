'use strict'

var React = require('react')
import { Row, Col, Input, Button } from 'react-bootstrap'

module.exports = React.createClass({
  search: function(e) {
    console.log(this.refs.search.getDOMNode().value)
    // let elts = []
    //     this.state.OEM.elements.map((d,i)=>{
    //       if(d.virtualDeviceId.indexOf(e.target.value)>-1) {
    //         elts.push(d)
    //       }
    //     })
    // Actions.setOEMElements(elts)
  },
  render: function() {
    return (
      <Row className="filters-rules filters">
        <Col md={7}>
          <input type="search" ref="search" placeholder="Search.."/>
        </Col>
        <Col md={3}>
          <Input className="filter-select" type="select"><option>Filter:</option></Input>
        </Col>
        <Col md={2}>
          <Button className="filter-submit" onClick={this.search}>Search</Button>
        </Col>
      </Row>
    )
  }
})
