'use strict'

var React = require('react')
  , Mixins = require('../../mixins/Mixins')
  , Rules = require('../../services/Rules')
import { Row, Col, FormControls, Input, Glyphicon } from 'react-bootstrap'

function getRule() { return { 
  rules: Rules.get_rules(),
  rule: Rules.get_rule(),
  range : Rules.get_rule().enabled,
  presence: Rules.get_rule().eventCondition.operand
} }

module.exports = React.createClass({
  mixins: [Mixins(getRule)],
  title: function(txt) { return ( <h2 className="rule-title">{txt}</h2> ) },
  statusChange: function(e) { this.setState({range:e.target.value}) },
  presenceChange: function(e) { this.setState({presence:e.target.value}) },
  virtualDevice: function(vd) {
    console.log('VD selected : ',vd)
  },
  render: function() {
    let r = this.state.rule,
        rules = this.state.rules
    return (
      <div className="rule" id="rule">
        <form className="form-horizontal">
          <Row className="rule-header-row">
          <Col md={12}>{this.title(r.ruleName)}</Col>
            <FormControls.Static label="Rule ID" labelClassName="col-md-2 sub-label-rule" wrapperClassName="col-md-5" value={r.ruleId}/>
            <FormControls.Static label="Type" labelClassName="col-md-2 sub-label-rule" wrapperClassName="col-md-5" value={r.ruleType}/>
          </Row>
          <Row>
            <Col md={2}>{this.title('Status')}</Col>
            <Col md={1} className="vertical-co-title"><input type="range" min="0" max="1" value={this.state.range} onChange={this.statusChange}/></Col>
          </Row>
          <Row>
            <Col md={12}>{this.title('Trigger')}</Col>
            <Col md={2}><span className="txt-btn">if</span></Col>
            <Col md={1} className="trigger-label"><p className="form-control-static">Presence</p></Col>
            <Col md={1}>
              <Input className="form-control-static filter-select presence" type="select" value={this.state.presence} onChange={this.presenceChange}>
                <option value={true}>true</option>
                <option value={false}>false</option>
              </Input>
            </Col>
          </Row>
          <Row>
            <Col md={12}>{this.title('Commands')}</Col>
            <Col md={2}><span className="txt-btn">then</span></Col>
            <Col md={2}><Input className="filter-select commands" type="select"><option value="on">on</option><option value="off">off</option></Input></Col>
          </Row>
        </form>
        <Row>
          <Col md={8}>
            {this.title('Virtual Devices')}
            {
              rules.map( (vd,i) => {
                if(vd.ruleCategory!=='EVENT'){
                  return (
                    <div key={i} className="vd-wrapper">
                      <div className="vd-img inline"><img className="bulb-cell" src="/images/bulboff.png"/></div>
                      <div className="vd inline">
                        <div className="vid">{vd.commands[0].virtualDeviceId}</div>
                        <div className="tc">{vd.commands[0].thingCategory}</div>
                        <div className="tct">{vd.commands[0].thingCategoryType}</div>
                      </div>
                      <div className="vd-arrow inline"><Glyphicon className="cursor" glyph="menu-right" onClick={this.virtualDevice.bind(null,vd)}/></div>
                    </div>
                  )
                }
              })
            }
          </Col>
        </Row>
      </div>
    )
  }
})
