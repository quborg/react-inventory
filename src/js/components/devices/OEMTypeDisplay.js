'use strict'

var React = require('react'),
    Store = require('../../stores/Store'),
    TableOEM = require('../../templates/TableOEM')
import { Row, Col, FormControls } from 'react-bootstrap'


module.exports = React.createClass({
  elementDisplay: function() {
    let e = this.state.element
    return (
      <div className="device-type-display" id="device">
        <Row className="device-header">
          <Col md={2}>
            <img src="/images/device.png"/>
          </Col>
          <Col md={10}>
            <form className="form-horizontal">
              <FormControls.Static label="Name" labelClassName="col-md-2" wrapperClassName="col-md-10" value={e.name}/>
              <FormControls.Static label="ID" labelClassName="col-md-2" wrapperClassName="col-md-10" value={e.virtualDeviceId}/>
              <FormControls.Static label="OEM" labelClassName="col-md-2" wrapperClassName="col-md-10" value={e.oem}/>
              <FormControls.Static label="Type" labelClassName="col-md-2" wrapperClassName="col-md-10" value={e.deviceType}/>
            </form>
          </Col>
        </Row>
        <Row>
          <Col md={11}>
            <TableOEM/>
          </Col>
        </Row>
      </div>
    )
  },
  render: function() {
    return this.elementDisplay()
  }
})
