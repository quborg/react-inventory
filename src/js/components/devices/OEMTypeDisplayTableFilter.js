'use strict'

var React = require('react'),
    Actions = require('../../actions/Actions'),
    Store = require('../../stores/Store'),
    Mixins = require('../../mixins/Mixins'),
    TableOEM = require('../../templates/TableOEM'),
    DeviceTypeDisplay = require('./DeviceTypeDisplay')
import { Row, Col, FormControls, Input } from 'react-bootstrap'

function getOEM() {
  return {
    OEM: Store.getOEM(),
    o2d: Store.getDisplay(),
    elements: Store.getOEMElements()
  }
}

module.exports = React.createClass({
  mixins: [Mixins(getOEM)],
  search: function(e) {
    let elts = []
        this.state.OEM.elements.map((d,i)=>{
          if(d.virtualDeviceId.indexOf(e.target.value)>-1) {
            elts.push(d)
          }
        })
    Actions.setOEMElements(elts)
  },
  OEMDisplay: function() {
    let e = this.state.OEM,
        o2d = this.state.o2d
    if(!e) return (<div className="no-select">Nothing selected</div>)
    if(o2d) return (<DeviceTypeDisplay/>)
    return (
      <div className="oem-type-display" id="oem">
        <Row className="oem-filters filters">
          <Col md={3}>
            <Input className="filter-select" type="select"><option>Filter by: No filter</option></Input>
          </Col>
          <Col md={8}>
            <input type="search" placeholder="Search by Device ID" onChange={this.search}/>
          </Col>
        </Row>
        <Row className="oem-table">
          <Col md={11}>
            <TableOEM oem={this.state.elements}/>
          </Col>
        </Row>
      </div>
    )
  },
  render: function() {
    return this.OEMDisplay()
  }
})
