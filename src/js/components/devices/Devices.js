'use strict'

var $ = require('jquery')
  , React = require('react')
  , OEMTypeList = require('./OEMTypeList')
  , Actions = require('../../actions/Actions')
  , DevicesFilter = require('./DevicesFilter')
  , Devices = require('../../services/Devices')
  , DeviceTypeList = require('./DeviceTypeList')
  , DeviceTypeDisplay = require('./DeviceTypeDisplay')
  , OEMTypeDisplayTableFilter = require('./OEMTypeDisplayTableFilter')
import { Button, Glyphicon, FormControls } from 'react-bootstrap'

Devices.loadDevices()

module.exports = React.createClass({
  getInitialState: function() {
    return {
      type : 'd'
    }
  },
  filterResult: function(e) {
    e && this.state.type == 'd' ? Actions.setElementDevice(0) : ''
    !e && this.state.type == 'o' ? Actions.setOEM(0) : ''
    e ? this.setState({type:'o'}) : this.setState({type:'d'})
  },
  render: function() {
    let DevicesList = this.state.type == 'd' ? <DeviceTypeList/> : <OEMTypeList/>,
        DevicesDisplay = this.state.type == 'd' ? <DeviceTypeDisplay/> : <OEMTypeDisplayTableFilter/>
    return (
      <div className="devices" id="devices">
        <div className="nav devices" id="nav-devices">
          <DevicesFilter filterResult={this.filterResult}/>
          {DevicesList}
        </div>
        <div className="element" id="element">
          {DevicesDisplay}
        </div>
      </div>
    )
  }
})
