'use strict'

import { Navbar, NavbarBrand } from 'react-bootstrap'
import { Link, IndexLink } from 'react-router'
var React     = require('react')
  , UserMenu  = require('./UserMenu')
  , Auth      = require('../../services/Auth')

module.exports = React.createClass({
  getInitialState() { return { loggedIn: Auth.loggedIn() } },
  render: function() {
    return (
      <div className="nav navbar-fixed-top container">
        <Link to="/states" className="logo navbar-brand">
          <span className="logo-text">adminapp</span>
          <img src="/images/verizon.png" className="verizon-logo"/>
        </Link>
        {this.state.loggedIn && <UserMenu/>}
      </div>
    )
  }
})
