'use strict'

import { Nav, MenuItem, NavDropdown } from 'react-bootstrap'
var React = require('react')
  , Auth  = require('../../services/Auth')

module.exports = React.createClass({
  render: function() {
    return (
      <Nav pullRight>
        <img src="/images/user-small.png"/>
        <NavDropdown eventKey={5} title="Account" id="nav-dropdown">
          <MenuItem>Profile</MenuItem>
          <MenuItem>Settings</MenuItem>
          <MenuItem>Item 1</MenuItem>
          <MenuItem>Item 2</MenuItem>
          <MenuItem onClick={Auth.logout}>Logout</MenuItem>
        </NavDropdown>
      </Nav>
    )
  }
})
