'use strict'

import { Input, Button } from 'react-bootstrap'
import { Link, browserHistory } from 'react-router'
var React = require('react')
  , Auth  = require('../../services/Auth')

module.exports = React.createClass({
  getInitialState () { return { loggedIn: Auth.loggedIn(), error: false } },
  updateAuth(loggedIn) { this.setState({ loggedIn: loggedIn }) },
  componentWillMount () {
    Auth.onChange = this.updateAuth
    Auth.login()
    this.state.loggedIn ? browserHistory.replace('/states') : null 
  },
  login() {
    Auth.login(
      this.refs.user.refs.input.value,
      this.refs.pass.refs.input.value, 
      (loggedIn) => { loggedIn ? browserHistory.push('/states') : this.setState({ error: true }) }
    )
  },
  render: function() {
    return (
      <div className="sign-in">
        <h2>Sign in to continue</h2>
        <div className="user-image"><img src="/images/user.png"/></div>
        <form>
          <Input ref="user" type="email" bsSize="medium" placeholder="Email Address" />
          <Input ref="pass" type="password" bsSize="medium" placeholder="Password" />
          {this.state.error && (<span className="red text-left">Bad login information</span>)}
          <Link to="#" className="forgot-password">Forgot password?</Link>
          <Button bsStyle="primary" bsSize="large" block onClick={this.login}>
            Log in
          </Button>
          <Button bsStyle="primary" block>Create account</Button>
        </form>
      </div>
    )
  }
})
