'use strict'

import Func from '../helpers/Func'

module.exports = {
  login(user, pass, cb) {
    cb = arguments[arguments.length - 1]
    if (localStorage.token) {
      if (cb) cb(true)
      this.onChange(true)
      return
    }
    Func.pretendRequest(user, pass, (res) => {
      if (res.authenticated) {
        localStorage.token = res.token
        localStorage.user = user
        if (cb) cb(true)
        this.onChange(true)
      } else {
        if (cb) cb(false)
        this.onChange(false)
      }
    })
  },

  getToken() { return localStorage.token },

  logout(cb) {
    delete localStorage.token
    delete localStorage.user
    if (cb) cb()
    this.onChange(false)
  },

  loggedIn() { return !!localStorage.token },

  onChange() {}
}
