'use strict'

var React = require('react'),
    ReactDOM = require('react-dom'),
    Routes = require('./site/Routes')

ReactDOM.render(Routes, document.getElementById('content'))
