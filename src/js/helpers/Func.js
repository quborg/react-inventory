'use strict'

import UUID from 'node-uuid'

var Func = {

  pretendRequest(user, pass, cb) {
    setTimeout(() => {
      if (user === 'admin' && pass === 'verizon')
        cb({
          authenticated: true,
          token: UUID.v4()
        })
      else cb({ authenticated: false })
    }, 0)
  }

}

module.exports = Func;
