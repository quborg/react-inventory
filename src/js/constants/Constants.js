module.exports = {
  OEM: 'OEM',
  RULE: 'RULE',
  RULES: 'RULES',
  DISPLAY: 'DISPLAY',
  DEVICES: 'DEVICES',
  OEMELEMENTS: 'OEMELEMENTS',
  RULE_DISPLAY: 'RULE_DISPLAY',
  ELEMENT_DEVICE: 'ELEMENT_DEVICE'
}
