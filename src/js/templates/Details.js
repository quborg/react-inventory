'use strict'

var _ = require('lodash'),
    React = require('react'),
    Store = require('../stores/Store'),
    Mixins = require('../mixins/Mixins')
import { Modal, Row, Col, FormControls, Input } from 'react-bootstrap'

function getElement() { return { device: Store.getElementDevice() } }

module.exports = React.createClass({
  mixins: [Mixins(getElement)],
  render: function() {
    let p = this.props,
        e = this.state.device,
        t = e.things[p.i] || [],
        a = t.actions || ['..'],
        perimeters = t.thingCatagory == 'actuator'
                     ? (<Col md={12} className="actions-textarea">
                          <Input type="textarea" placeholder="Enter parameters"/>
                        </Col>)
                     : ''
    console.log(t)
    return (
      <Modal show={p.show} onHide={p.hide}>
        <Modal.Body>
          <div className="element-modal">
            <Row className="modal-spec">
              <div className="row-wrapper">
                <Col xs={2}>
                  <img src={t.icon}/>
                </Col>
                <Col xs={10}>
                  <form className="form-horizontal">
                    <FormControls.Static label="Name" labelClassName="col-md-4" wrapperClassName="col-md-8" value={e.name}/>
                    <FormControls.Static label="Category" labelClassName="col-md-4" wrapperClassName="col-md-8" value={_.capitalize(t.thingCatagory)}/>
                    <FormControls.Static label="ID" labelClassName="col-md-4" wrapperClassName="col-md-8" value={t.id}/>
                    <FormControls.Static label="Virtual Device ID" labelClassName="col-md-4" wrapperClassName="col-md-8" value={t.virtualId}/>
                  </form>
                </Col>
              </div>
            </Row>
            <Row className="modal-desc">
              <div className="row-wrapper">
                <Col md={12}>
                  <div className="desc-title">Description:</div>
                  <div className="desc-body">{t.description}</div>
                </Col>
              </div>
            </Row>
            <Row className="modal-actions">
              <div className="row-wrapper">
                <Col md={12} className="actions-select">
                  <Input className="actions" type="select" label="Select action:" labelClassName="col-md-2" wrapperClassName="col-md-5">
                    {a.map((o,i)=>{return (<option key={i} value={o}>{o}</option>)})}
                  </Input>
                </Col>
                { perimeters }
              </div>
            </Row>
            <Row className="modal-foot">
              <div className="row-wrapper">
                <Col md={6} className="space-btn-l">
                  {p.cancel}
                </Col>
                <Col md={6} className="space-btn-r">
                  {p.apply}
                </Col>
              </div>
            </Row>
          </div>
        </Modal.Body>
      </Modal>
    )
  }
})