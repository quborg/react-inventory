'use strict'

var $ = require('jquery')
  , React = require('react')
  , Link = require('react-router').Link
import { IndexLink } from 'react-router'
import { Nav, Glyphicon } from 'react-bootstrap'

module.exports = React.createClass({
  componentDidMount: function() { $('.navbar-fixed-side-left .glyphicon-plus-sign').css('top',$('html').height() - 75 + 'px') },
  disableLink: function(e) { e.preventDefault() },
  render: function() {
    let _classes = this.props.classes
    return (
      <div className="navbar-fixed-side navbar-fixed-side-left">
        <Nav>
          <li className={_classes[0]+" cursor"} onClick={this.props.list.bind(null,0)}>
            <Link to="/inventory" onClick={this.disableLink}>Inventory</Link>
            <Glyphicon glyph="menu-right"/>
          </li>
          <li className={_classes[1]+" cursor"} onClick={this.props.list.bind(null,1)}>
            <Link to="/devices" onClick={this.disableLink}>States</Link>
            <Glyphicon glyph="menu-right"/>
          </li>
          <li className={_classes[2]+" cursor"} onClick={this.props.list.bind(null,2)}>
            <Link to="/rules" onClick={this.disableLink}>Rules</Link>
            <Glyphicon glyph="menu-right"/>
          </li>
        </Nav>
      </div>
    )
  }
})
