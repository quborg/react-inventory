'use strict'

var React = require('react'),
    Assign = require('object-assign'),
    Link = require('react-router').Link,
    Mixins = require('../mixins/Mixins'),
    Actions = require('../actions/Actions'),
    Devices = require('../services/Devices')
import { Button, Glyphicon, Badge, FormControls } from 'react-bootstrap'

function getElement() { return { element: Devices.get_element() } }

module.exports = React.createClass({
  mixins: [Mixins(getElement)],
  getInitialState: function() { 
    return {
      devices: this.props.data,
      subColumns: [{name:".." ,_class:".."}]
    }
  },
  firstListSelection: function(i,e) {
    let devices = Assign([],this.state.devices)
    devices.map( (v,k) => { devices[k].glyph="plus-sign" } )
    devices[i].glyph = "minus-sign"
    this.setState({subColumns:e.elements}, function(){
      document.getElementById("second-list").style.top = (Number(i)*48)+'px'
      this.secondListSelection(0,this.state.subColumns[0])
    })
  },
  secondListSelection: function(i,e) {
    let subColumns = Assign([],this.state.subColumns)
    subColumns.map( (v,k) => { subColumns[k]._class="" } )
    subColumns[i]._class="btn btn-grey"
    this.setState({subColumns:subColumns})
    Actions.setElement([e])
  },
  componentDidMount: function(){
    this.firstListSelection(0,this.state.devices[0])
  },
  render: function() {
    return (
      <div>
        <div id="first-list" className="column-list">
          {this.state.devices.map((e,i)=>{
            return (
              <Button key={i} bsStyle="primary" onMouseOver={this.firstListSelection.bind(null,i,e)}>
                <Glyphicon glyph={e.glyph}/>
                <span className="span-st-txt">{e.name}</span>
                <Glyphicon glyph="menu-right"/>
                <Badge>{e.elements.length}</Badge>
              </Button>
            )
          })}
        </div>
        <div id="second-list" className="column-list">
          {this.state.subColumns.map((e,i)=>{
            return (
              <Button key={i} className={e._class} onMouseOver={this.secondListSelection.bind(null,i,e)}>
                {e.name}
              </Button>
            )
          })}
        </div>
        <div className="inv-element">
          <form className="form-horizontal">
            {this.state.element.map((e,i)=>{
              let things = e.things == undefined ? [] : e.things
              return (
                <div key={i}>
                  <div className="element-title">Device Information</div>
                  <FormControls.Static label="Name :" labelClassName="col-xs-5" wrapperClassName="col-xs-7" value={e.name}/>
                  <FormControls.Static label="ID :" labelClassName="col-xs-5" wrapperClassName="col-xs-7" value={e.virtualDeviceId}/>
                  <FormControls.Static label="OEM :" labelClassName="col-xs-5" wrapperClassName="col-xs-7" value={e.oem}/>
                  <FormControls.Static label="Type :" labelClassName="col-xs-5" wrapperClassName="col-xs-7" value={e.deviceType}/>
                  <div className="element-title">Things</div>
                  {things.map((e,i)=>{
                    return ( <div key={i} className="inv-things"> <Glyphicon glyph="signal"/> {e.thingCatagory}</div> )
                  })}
                </div>
              )
            })}
          </form>
        </div>
      </div>
    )
  }
})
