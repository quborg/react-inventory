'use strict'

var React = require('react')

module.exports = React.createClass({
  getInitialState: function() {
    return {
      filter: 'Device Type',
      classD: '',
      classO: ''
    }
  },
  filterSelect: function(e) {
    let f = e ? 'OEM' : 'Device Type'
    e ? this.setState({classD:'',classO:'selected'}) : this.setState({classD:'selected',classO:''})
    this.setState({filter:f})
  },
  render: function() {
    return (
      <div className="filter filter-device-type">
        <NavDropdown eventKey={2} title={'Filter by: '+this.state.filter} id="filter-devices">
          <MenuItem eventKey="2.1" onClick={this.filterSelect.bind(null,0)} className={this.state.classD}>Device Type</MenuItem>
          <MenuItem eventKey="2.2" onClick={this.filterSelect.bind(null,1)} className={this.state.classO}>OEM</MenuItem>
        </NavDropdown>
      </div>
    )
  }
})
