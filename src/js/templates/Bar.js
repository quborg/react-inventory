'use strict'

var React = require('react')

module.exports = React.createClass({
  render: function() {
    if(this.props.center!==undefined)
      return (
        <div className="bar">
          <div className="center">{this.props.center}</div>
        </div>
      )
    else
      return (
        <div className="bar">
          <div className="first">{this.props.first}</div>
          <div className="second">{this.props.second}</div>
        </div>
      )
  }
})
