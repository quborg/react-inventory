'use strict'

var React = require('react')
  , AdminMenu = require('../components/user/AdminMenu')

module.exports = React.createClass({
  render: function() {
    return (
      <div className="access" id="access">
        <AdminMenu/>
        {this.props.children}
      </div>
    )
  }
})
