'use strict'

var React = require('react')
import { Breadcrumb, BreadcrumbItem, Glyphicon } from 'react-bootstrap'

module.exports = React.createClass({
  render: function() {
    <ol className="breadcrumb">
      <li><a href="/"><Glyphicon glyph="home"/>Home</a></li>
      <li className="active">now</li>
    </ol>
    
  }
})
