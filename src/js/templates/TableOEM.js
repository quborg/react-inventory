'use strict'

var React = require('react'),
    Actions = require('../actions/Actions')
import { Table, Button } from 'react-bootstrap'

module.exports = React.createClass({
  thead: function() {
    return (
      <tr>
        <th column="name"><em>Name</em></th>
        <th column="stat"><em>Device Type</em></th>
        <th column="devi"><em>Device ID</em></th>
        <th column="deta"><em></em></th>
      </tr>
    )
  },
  tbody: function() {
    return (
      <tbody>
        {this.props.oem.map((r,i)=>{
          return(
            <tr key={i}>
              <td>{r.name}</td>
              <td>{r.deviceType}</td>
              <td>{r.virtualDeviceId}</td>
              <td className="txt-center"><a href="#" className="btn-details" onClick={this.oem2device.bind(null,r)}>details</a></td>
            </tr>
        )})}
      </tbody>
    )
  },
  oem2device: function(r) { 
    Actions.setElementDevice(r)
    Actions.setDisplay(1)
  },
  render: function() {
    let classes = "table-striped table-condensed table-hover"
    return (
      <div>
        <Table className={classes}>
          <thead>{this.thead()}</thead>
          {this.tbody()}
        </Table>
      </div>
    )
  }
})
