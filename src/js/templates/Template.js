'use strict'

import { Grid, Row } from 'react-bootstrap'
import { browserHistory } from 'react-router'
var React = require('react')
  , Menu  = require('./Menu')
  , Auth  = require('../services/Auth')
  , URLS  = {
      0: '/inventory',
      1: '/states',
      2: '/rules'
    }

module.exports = React.createClass({
  getInitialState () {
    return {
      loggedIn: Auth.loggedIn(),
      classes: ['','active','']
    }
  },
  activateLink: function(l) { 
    let classes = [
          l==0?'active':'',
          l==1?'active':'',
          l==2?'active':''
        ]
    this.props.history.push(null,URLS[l])
    this.setState({classes})
  },
  componentWillMount () { this.state.loggedIn ? null : browserHistory.replace('/') },
  render: function() {
    return (
      <div className="template" id="template">
        <Menu classes={this.state.classes} list={this.activateLink}/>
        {this.props.children}
      </div>
    )
  }
})
