'use strict'

var express = require('express'),
    request = require('request'),
    path = require('path'),
    app = express(),
    list = 'http://tspace-dev-vm1.eastus.cloudapp.azure.com:8081/devices/list',
    rules = 'http://tspace-dev-vm1.eastus.cloudapp.azure.com:8081/devices/rules/list',
    meta = 'http://tspace-dev-vm1.eastus.cloudapp.azure.com:8081/devices/rules/meta'

app.use(express.static(path.join(__dirname, 'assets')))

// Add crossDomain Rest routes
app.get('/data-list', function(req,res){
  request( list , function (error, response, body) {
    res.send(body)
  })
})

app.get('/data-rules', function(req,res){
  request( rules , function (error, response, body) {
    res.send(body)
  })
}) 

app.get('/*', function(req,res){
  res.sendFile(__dirname + '/assets/index.html')
})

// Route not found -- Set 404
// app.get('*', function(req, res) {
//   res.send('route : Sorry this page does not accessible!')
// })

app.listen(3300, function(){
  console.log('Server is Up and Running at Port : 3300')
})
