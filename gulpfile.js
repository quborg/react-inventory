var gulp = require('gulp')
  , browserify = require('browserify')
  , source = require('vinyl-source-stream')
  , browserSync = require('browser-sync')

gulp.task('browserify', function() {
  browserify('src/js/main.js')
    .transform('babelify', {presets: ["es2015", "react"]})
    .bundle()
    .pipe(source('main.js'))
    .pipe(gulp.dest('assets/js'))
    .pipe(browserSync.stream({once: true}))
})

gulp.task('index', function() { gulp.src('src/index.html').pipe(gulp.dest('assets')) })

gulp.task('images', function() { gulp.src('src/images/**/*.*').pipe(gulp.dest('assets/images')) })

gulp.task('styles', function() { gulp.src('src/css/**/*.*').pipe(gulp.dest('assets/css')) })

gulp.task('browsersync', function() {
  browserSync.init({
    proxy: { target: 'localhost:3300' },
    files: ['./assets'],
    port: 3000
  })
})

gulp.task('default',['browserify','index','images','styles','browsersync'], function() {
  gulp.watch('src/js/**/*.*',['browserify'], function () { browserSync.reload() })
  gulp.watch('src/index.html',['index'], function () { browserSync.reload() })
  gulp.watch('src/images/**/*.*',['images'], function () { browserSync.reload() })
  gulp.watch('src/css/**/*.*',['styles'], function () { browserSync.reload() })
})
